package com.sumerge.superSonice;

import com.sumerge.superSonice.core.business.BaseService.BaseItemService;
import com.sumerge.superSonice.core.business.BaseService.BaseOrderService;
import com.sumerge.superSonice.core.entity.OrderItem;
import com.sumerge.superSonice.core.repository.ItemRepository;
import com.sumerge.superSonice.core.repository.CustomerRepository;
import com.sumerge.superSonice.core.repository.OrderItemRepository;
import com.sumerge.superSonice.core.repository.OrderRepository;
import com.sumerge.superSonice.shell.business.service.ItemService;
import com.sumerge.superSonice.shell.business.service.OrderService;
import com.sumerge.superSonice.shell.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class AppConfiguration {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @Bean public BCryptPasswordEncoder bCryptPasswordEncoder() {    return new BCryptPasswordEncoder(); }

    @Bean
    public BaseOrderService orderService(){

        return new OrderService(orderRepository,orderItemRepository);
    }
    @Bean
    public BaseItemService itemService(){

        return new ItemService(itemRepository);
    }
    @Bean
    public UserService userService(){
        return new UserService(customerRepository);
    }
}
