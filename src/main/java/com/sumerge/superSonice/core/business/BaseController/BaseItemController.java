package com.sumerge.superSonice.core.business.BaseController;

import com.sumerge.superSonice.core.entity.Item;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/items")
public interface BaseItemController {

    @GetMapping
    List<Item> getAllItems();

    @PostMapping
    Item addItem(Item item);

    @DeleteMapping
    void deleteItem(long id);

    @PutMapping
    Item updateItem(Item item);
}
