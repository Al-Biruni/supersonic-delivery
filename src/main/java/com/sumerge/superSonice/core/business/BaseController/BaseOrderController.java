package com.sumerge.superSonice.core.business.BaseController;

import com.sumerge.superSonice.core.business.dto.OrderDTO;
import com.sumerge.superSonice.core.entity.Order;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/orders")
public interface BaseOrderController {

    @GetMapping
     List<OrderDTO> getAllOrders();
    @PostMapping
    Order addOrder(Order order);
    @PutMapping
     Order updateOrder(Order order);
    @DeleteMapping
    void deleteOrder(long id);

    @RequestMapping("/{orderId}")
    @GetMapping
    public Order getOrderById(@PathVariable("orderId") long id);

}
