package com.sumerge.superSonice.core.business.BaseService;

import com.sumerge.superSonice.core.entity.Item;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BaseItemService {
    List<Item> getItems();
    Item addItem(Item item);
    Item updateItem(Item item);
    void deleteItem(long id);
}
