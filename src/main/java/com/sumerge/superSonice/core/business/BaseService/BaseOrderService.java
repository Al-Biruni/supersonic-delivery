package com.sumerge.superSonice.core.business.BaseService;

import com.sumerge.superSonice.core.business.dto.OrderDTO;
import com.sumerge.superSonice.core.entity.Order;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface BaseOrderService {

    List<Order> getOrders();
    Order addOrder(Order Order);
    Order updateOrder(Order Order);
    void deleteOrder(long id);

    Order findOrderById(long id);
}
