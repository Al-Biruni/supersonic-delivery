package com.sumerge.superSonice.core.business.dto;

import com.sumerge.superSonice.core.entity.Order;
import com.sumerge.superSonice.core.entity.OrderItem;

import java.util.List;

public class OrderDTO {



        private Long id;
        private List<OrderItem> orderItems;
        private long userID;
        private int total;

        public OrderDTO(Order order){
            this.id = order.getId();
            this.orderItems = order.getOrderItems();
            this.userID = order.getCustomer().getId();
            this.total = order.getTotal();
        }

    public Long getId() {
        return id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public long getUserID() {
        return userID;
    }

    public int getTotal() {
        return total;
    }
}
