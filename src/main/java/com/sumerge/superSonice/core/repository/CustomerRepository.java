package com.sumerge.superSonice.core.repository;

import com.sumerge.superSonice.core.business.dto.CustomerDto;
import com.sumerge.superSonice.core.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "packages", path = "packages")
public interface CustomerRepository extends CrudRepository<Customer,Long> {
    Customer findByUserName(String s);


}
