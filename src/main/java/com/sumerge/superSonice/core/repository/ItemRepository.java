package com.sumerge.superSonice.core.repository;



import com.sumerge.superSonice.core.entity.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
     List<Item> findByRestaurant(String restaurant);
}
