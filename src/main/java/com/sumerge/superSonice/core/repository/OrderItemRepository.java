package com.sumerge.superSonice.core.repository;

import com.sumerge.superSonice.core.entity.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "packages", path = "packages")
public interface OrderItemRepository extends CrudRepository<OrderItem,Long> {
     List<OrderItem> findByOrderId(Long id);
}
