package com.sumerge.superSonice.core.repository;

import com.sumerge.superSonice.core.entity.Order;
import org.springframework.data.repository.CrudRepository;


public interface OrderRepository extends CrudRepository<Order, Long> {
}
