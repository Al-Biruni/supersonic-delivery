package com.sumerge.superSonice.shell.business.controller;

import com.sumerge.superSonice.core.business.dto.CustomerDto;
import com.sumerge.superSonice.core.entity.Customer;
import com.sumerge.superSonice.shell.business.service.CustomerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class CustomerController {
 private CustomerService customerService;

    @PostMapping
    public long addCustomer(CustomerDto customerDto){
        return customerService.saveDto(customerDto);
    }

}
