package com.sumerge.superSonice.shell.business.controller;

import com.sumerge.superSonice.core.business.BaseController.BaseItemController;
import com.sumerge.superSonice.core.business.BaseService.BaseItemService;
import com.sumerge.superSonice.core.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
@RestController
@RequestMapping("/items")
public class ItemController implements BaseItemController {
    private  BaseItemService itemService;
@Autowired
    public ItemController(BaseItemService itemService) {
        this.itemService = itemService;
    }


    public List<Item> getAllItems() {
        return  itemService.getItems();
    }


    public Item addItem(@RequestBody Item item) {
        return itemService.addItem(item);
    }

    public void deleteItem(long id) {
    itemService.deleteItem(id);

    }

    @Override
    public Item updateItem(Item item) {
        return itemService.updateItem(item);
    }
}
