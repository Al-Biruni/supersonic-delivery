package com.sumerge.superSonice.shell.business.controller;

import com.sumerge.superSonice.core.business.BaseService.BaseOrderService;
import com.sumerge.superSonice.core.business.dto.OrderDTO;
import com.sumerge.superSonice.core.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final BaseOrderService orderService;
    @Autowired
    public OrderController(BaseOrderService orderService) {
        this.orderService = orderService;
    }


    @GetMapping
    public List<OrderDTO> getAllOrders() {

        return orderService.getOrders().stream().map(order -> new OrderDTO(order)).collect(Collectors.toList());
    }

    public Order addOrder(Order order) {
        return orderService.addOrder(order);
    }

    public Order updateOrder(Order order) {
        return orderService.updateOrder(order);
    }

    public void deleteOrder(long id) {
        orderService.deleteOrder(id);
    }


    public Order getOrderById(long id) {
        Order order = orderService.findOrderById(id);
        return order;
    }

}
