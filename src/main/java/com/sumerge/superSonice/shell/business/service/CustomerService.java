package com.sumerge.superSonice.shell.business.service;

import com.sumerge.superSonice.core.business.dto.CustomerDto;
import com.sumerge.superSonice.core.entity.Customer;
import com.sumerge.superSonice.core.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
 private final CustomerRepository customerRepository;
@Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    public long saveDto(CustomerDto customerDto){
        return customerRepository.save(new Customer(customerDto)).getId();
    }
}
