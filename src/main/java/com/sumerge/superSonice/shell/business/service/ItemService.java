package com.sumerge.superSonice.shell.business.service;

import com.sumerge.superSonice.core.business.BaseService.BaseItemService;
import com.sumerge.superSonice.core.entity.Item;
import com.sumerge.superSonice.core.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;



public class ItemService implements BaseItemService{

    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }


    public List<Item> getItems() {
        Iterable<Item> items = itemRepository.findAll();
        List<Item> itemsList = new ArrayList<>();
        items.forEach(item -> itemsList.add(item));
        return itemsList;
    }


    public Item addItem(Item item) {
        return itemRepository.save(item);
    }


    public Item updateItem(Item item) {
        return itemRepository.save(item);
    }


    public void deleteItem(long id) {
        itemRepository.deleteById(id);
    }

}
