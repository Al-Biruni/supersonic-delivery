package com.sumerge.superSonice.shell.business.service;

import com.sumerge.superSonice.core.business.BaseService.BaseOrderService;
import com.sumerge.superSonice.core.business.dto.OrderDTO;
import com.sumerge.superSonice.core.entity.Order;
import com.sumerge.superSonice.core.entity.OrderItem;
import com.sumerge.superSonice.core.repository.OrderItemRepository;
import com.sumerge.superSonice.core.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class OrderService implements BaseOrderService {
    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
@Autowired
    public OrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository) {
        this.orderRepository = orderRepository;
    this.orderItemRepository = orderItemRepository;
}


    public List<Order> getOrders(){


    return (List<Order>) orderRepository.findAll();
    }

    @Override
    public Order addOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(long id) {
    orderRepository.deleteById(id);

    }

    @Override
    public Order findOrderById(long id) {
    Order order =  orderRepository.findById(id).get();
    order.setOrderItems(orderItemRepository.findByOrderId(order.getId()));



    return order;
    }
}
