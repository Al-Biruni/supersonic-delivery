package com.sumerge.superSonice.shell.business.service;

import com.sumerge.superSonice.core.entity.Customer;
import com.sumerge.superSonice.core.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;

public class UserService implements UserDetailsService  {
    private final CustomerRepository customerRepository;

    @Autowired
    public UserService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByUserName(s);
        User springUser = new User(customer.getUserName(), customer.getPassword(),new ArrayList<>());
        return springUser;
    }


}
